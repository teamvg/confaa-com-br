<!--
77.......77...77777..
.77.....77..77.....7. AGÊNCIA DIGITAL
..77...77..77........ ATENDIMENTO@VGWEB.COM.BR
...77.77...77....7777 +55 82 99165-5274
....777.....77....777 www.VGAD.com.br
.....7........7777..7
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CONFAA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?php echo bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>

    <!-- carousel -->
    <link rel="stylesheet" href="<?php echo bloginfo('template_url') ?>/assets/css/swiper.css">
    <script src="<?php echo bloginfo('template_url') ?>/assets/js/swiper.min.js"></script>
    <!-- carousel -->

    <link rel="shortcut icon" href="<?php echo bloginfo('template_url') ?>/assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo bloginfo('template_url') ?>/assets/img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo bloginfo('template_url') ?>/assets/css/app.css">

</head>

<body>

    <nav class="navbar navbar-expand-md bg-light navbar-light">
        <div class="container">
            <a class="navbar-brand" href="../../confaa.com.br"><img src="<?php echo bloginfo('template_url') ?>/assets/img/logo.png" alt=""> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="confraria">A Confraria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Arapiraca</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Cultura</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ASA</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Personalidades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Envie fotos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Aniversariantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Blogs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Galeria de fotos</a>
                    </li>
                    <a href="#">
                        <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/whats.svg">
                    </a>
                    <a href="#">
                        <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/face.svg">
                    </a>
                    <a href="#">
                        <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/you.svg">
                    </a>
                    <a href="#">
                        <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/inst.svg">
                    </a>
                </ul>
            </div>
        </div>
    </nav>
