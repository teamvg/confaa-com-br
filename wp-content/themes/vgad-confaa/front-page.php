<?php echo get_header() ?>

<div class="container-fluid">

    <div class="row">

        <div id="demo" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/banner.png"  class="img-fluid" alt="Responsive image">
                </div>
                <div class="carousel-item">
                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/banner.png"  class="img-fluid" alt="Responsive image">
                </div>
                <div class="carousel-item">
                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/banner.png"  class="img-fluid" alt="Responsive image">
                </div>
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <!-- <span class="carousel-control-prev-icon"></span> -->
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <!-- <span class="carousel-control-next-icon"></span> -->
            </a>
        </div>

    </div>

</div>

<div class="container arg">
    <div class="text-center">
        <img src="<?php echo bloginfo('template_url') ?>/assets/img/arg.png"  class="img-fluid" alt="Responsive image">
    </div>
</div>

<div class="container base-bg">
    <div class="row">
        <div class="text-center">
            <h1>Escândalo! Tainá do Dr. Lauro gasta 574 mil com curso e empresa de consultoria</h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-sm-6 relative big-grid">
            <div class="grid-inner">
                <a href="#">
                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                    <div class="big-grid-overlay layed dest">
                        <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-6 relative big-grid">
            <div class="row">

                <div class="col-sm-6 relative big-grid ">
                    <div class="grid-inner">
                        <a href="#">
                            <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                            <div class="big-grid-overlay layed">
                                <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-6 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                            <div class="big-grid-overlay layed">
                                <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-6 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                            <div class="big-grid-overlay layed">
                                <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-6 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                            <div class="big-grid-overlay layed">
                                <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="container base-bg patro">
    <div class="row">
        <div class="col-sm-4 col-4">
            <div class="text-center">
                <img src="<?php echo bloginfo('template_url') ?>/assets/img/p1.png"  class="img-fluid" alt="Responsive image">
            </div>
        </div>
        <div class="col-sm-4 col-4">
            <div class="text-center">
                <img src="<?php echo bloginfo('template_url') ?>/assets/img/p2.png"  class="img-fluid" alt="Responsive image">
            </div>
        </div>
        <div class="col-sm-4 col-4">
            <div class="text-center">
                <img src="<?php echo bloginfo('template_url') ?>/assets/img/p3.png"  class="img-fluid" alt="Responsive image">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid base-bg bg-niver">
    <div class="container">

        <div class="text-center">
            <a href="#">
                <h2>ANIVERSARIANTES</h2>
            </a>
        </div>

        <div class="row">
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/woman.png" class="img-fluid" alt="Responsive image">
                        <p>10/10</p>
                        <h4>Jenis Joplin</h4>
                    </div>
                </div>


                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>

            </div>
        </div>

    </div>
</div>

<div class="container-fluid cul">
    <div class="row">

        <div class="container">
            <div class="text-center">
                <h2>CULTURA</h2>
            </div>
        </div>
        <div class="col-sm-6 relative big-grid dest">
            <div class="">
                <div class="grid-inner">
                    <a href="#">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                        <div class="big-grid-overlay layed">
                            <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-6 relative big-grid dest">
            <div class="">
                <div class="grid-inner">
                    <a href="#">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                        <div class="big-grid-overlay layed">
                            <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-4 relative big-grid dest">
            <div class="">
                <div class="grid-inner">
                    <a href="#">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                        <div class="big-grid-overlay layed">
                            <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-4 relative big-grid dest">
            <div class="">
                <div class="grid-inner">
                    <a href="#">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                        <div class="big-grid-overlay layed">
                            <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-4 relative big-grid dest">
            <div class="">
                <div class="grid-inner">
                    <a href="#">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                        <div class="big-grid-overlay layed">
                            <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- patrocinadores -->
<div class="container patro">
    <div class="row">
        <div class="col-sm-4 col-4">
            <div class="text-center">
                <img src="<?php echo bloginfo('template_url') ?>/assets/img/p1.png"  class="img-fluid" alt="Responsive image">
            </div>
        </div>
        <div class="col-sm-4 col-4">
            <div class="text-center">
                <img src="<?php echo bloginfo('template_url') ?>/assets/img/p2.png"  class="img-fluid" alt="Responsive image">
            </div>
        </div>
        <div class="col-sm-4 col-4">
            <div class="text-center">
                <img src="<?php echo bloginfo('template_url') ?>/assets/img/p3.png"  class="img-fluid" alt="Responsive image">
            </div>
        </div>
    </div>
</div>
<!-- patrocinadores -->

<div class="container-fluid asa">
    <div class="row">

        <div class="col-sm-12 base-asa">
            <div class="text-center">
                <div class="row">
                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/star1.png">
                    <h2>ASA</h2>
                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/star2.png">
                </div>
            </div>
        </div>

        <!--  -->
        <div class="col-sm-4">
            <div class="row">
                <div class="col-12 relative big-grid dest">
                    <div class="">
                        <div class="grid-inner">
                            <a href="#">
                                <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png" class="img-fluid" alt="Responsive image">
                                <div class="big-grid-overlay layed">
                                    <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png"  class="img-fluid" alt="Responsive image">
                                </div>
                                <div class="col-sm-6">
                                    <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!--  -->

        <div class="col-sm-4">
            <div class="row">

                <div class="col-sm-12 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png"  class="img-fluid" alt="Responsive image">
                                </div>
                                <div class="col-sm-6">
                                    <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-12 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png"  class="img-fluid" alt="Responsive image">
                                </div>
                                <div class="col-sm-6">
                                    <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-12 relative big-grid">
                    <div class="grid-inner">
                        <a href="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <img src="<?php echo bloginfo('template_url') ?>/assets/img/modelo.png"  class="img-fluid" alt="Responsive image">
                                </div>
                                <div class="col-sm-6">
                                    <h3>Ricardo Nezinho é reeleito deputado estadual com o segundo maior número de votos</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-4 fute">
            <div class="boarde-solid">
                <div class="text-center">
                    <div class="row">
                        <h3>Último Jogo</h3>
                    </div>
                </div>

                <div class="text-center">
                    <div class="row">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/asa.png">
                        <h2>2</h2>
                        <h2>X</h2>
                        <h2>2</h2>
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/asa.png">
                    </div>
                    <p>Estádio Municipal | Arapiraca 19h00</p>
                </div>

                <hr>

                <div class="text-center">
                    <div class="row">
                        <h3>Próximo Jogo</h3>
                    </div>
                </div>

                <div class="text-center">
                    <div class="row">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/asa.png">
                        <h2>2</h2>
                        <h2>X</h2>
                        <h2>2</h2>
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/asa.png">
                    </div>
                    <p>Estádio Municipal | Arapiraca 19h00</p>
                </div>

                <div class="text-center">
                    <div class="row">
                        <button type="button" class="btn btn-success">VER MAIS</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="container-fluid base-bg bg-art">
    <div class="container">

        <div class="text-center">
            <a href="#">
                <h2>ARTISTAS</h2>
            </a>
        </div>

        <div class="row">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis JoplinJenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/art.png" class="img-fluid" alt="Responsive image">
                        <h4>Jenis Joplin</h4>
                        <p>Voz & Violão</p>
                        <p>82 9 9999-9696</p>
                    </div>
                </div>

                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>

            </div>
        </div>

    </div>
</div>

<!-- script -->

<script>
var swiper = new Swiper('.swiper-container', {
    slidesPerView: 5,
    spaceBetween: 30,
    // init: false,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        1024: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 30,
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
</script>


<?php echo get_footer() ?>
