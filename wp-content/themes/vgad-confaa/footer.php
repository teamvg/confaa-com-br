<footer class="base-bg">
    <div class="row">

        <div class="col-md-6 offset-md-3">
            <div class="text-center">
                <h2>CONTATO</h2>
            </div>

            <input type="text" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Seu nome">

            <input type="email" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Email">

            <input type="tel" maxlength="12" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Telefone">

            <textarea class="form-control" aria-label="With textarea" placeholder="Mensagem"></textarea>

            <div class="text-center">
                <button type="button" class="btn btn-success">ENVIAR</button>
            </div>

        </div>

        <div class="col-md-6 offset-md-3">
            <div class="text-center">
                <a href="#">
                    <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/whats.svg">
                </a>
                <a href="#">
                    <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/face.svg">
                </a>
                <a href="#">
                    <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/you.svg">
                </a>
                <a href="#">
                    <img class="social" src="<?php echo bloginfo('template_url') ?>/assets/img/inst.svg">
                </a>
            </div>
            <div class="text-center">
                <p>confaa 2018.</p>
            </div>
        </div>

    </div>
</footer>

</body>
</html>
